<?php 

	function conexion($usuario, $pass){
		try {
			$conexion = new PDO('mysql:host=localhost;dbname=prueba_extreme', $usuario, $pass);
			return $conexion;
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	function datos_vacios($datos){
		$vacio = false;
		$length = count($datos);
		for ($i = 0; $i < $length; $i++){
			if (empty($datos[$i])){
				$vacio = true;
				break;
			}
		}
		return $vacio;
	}
	function limpiar($datos){
		$length = count($datos);
		for($i = 0; $i < $length; $i++){
			if($i != 2){
				# La funcion htmlspecialchars() convierte los caracteres predefinidos menor que y mayor que en entidades html
				$datos[$i] = htmlspecialchars($datos[$i]);
				# Remueve espacios de ambos lados de un string
				$datos[$i] = trim($datos[$i]);
				# Remueve la barra invertida
				$datos[$i] = stripcslashes($datos[$i]);
			}
		}
		return $datos;
	}

	function verificar_session(){
		if(!isset($_SESSION['cod_usua'])){
			header('location: login.php');
		}
	}
?>