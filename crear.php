<?php
	require('funciones.php');
	require('clases/clases.php');
	if (isset($_POST['crear'])){
		$fecha_limite = '';
		$datos = array(
			$tipo_pqr = $_POST['tipo_pqr'],
			$asunto_pqr = $_POST['asunto_pqr'],
			$usuario = $_POST['usuario']);
		if(datos_vacios($datos) == false){
			$resultados = usuarios::verificar($datos[2]);
			if (empty($resultados) == false){
				$ahora = date('Y-m-d');
				switch ($tipo_pqr) {
					case 'peticion':
						$fecha_limite = strtotime( $ahora. '+ 7 days');
						$fecha_limite = date('Y-m-d',$fecha_limite);
						break;
					case 'queja':
						$fecha_limite = strtotime( $ahora. '+ 3days');
						$fecha_limite = date('Y-m-d',$fecha_limite);
						break;
					case 'reclamo':
						$fecha_limite = strtotime( $ahora. '+ 2days');
						$fecha_limite = date('Y-m-d',$fecha_limite);
						break;
					default:
						break;
				}
				$con = conexion('root', '');
				$consulta = $con->prepare('insert into pqr(cod_pqr, tipo_pqr, asunto_pqr, usuario, estado, fecha_creacion, fecha_limite, cod_usua)
					values (null, :tipo_pqr, :asunto_pqr, :usuario, :estado, :fecha_creacion, :fecha_limite, :cod_usua)');
				$consulta->execute(array(
					':tipo_pqr' => $tipo_pqr,
					':asunto_pqr' => $asunto_pqr,
					':usuario' => $usuario,
					':estado' => 'Nuevo',
					':fecha_creacion' => $ahora,
					':fecha_limite' => $fecha_limite,
					':cod_usua' => $resultados[0]['cod_usua']
				));
				header('location: index.php');
			}
		}
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale<<=1.0">
	<title>Crear PQR</title>
	<link rel="stylesheet" href="css/index.css">
</head>
<body>
	<div class="contenedor-form">
		<h1>Crear PQR</h1>
		<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
			<select name="tipo_pqr" id="">
				<option value="">Seleccione el tipo de PQR</option>
				<option value="peticion">Peticion</option>
				<option value="queja">Queja</option>
				<option value="reclamo">Reclamo</option>
			</select>
			<textarea type="textaera" name="asunto_pqr" placeholder="Asunto del PQR" class="input-control"></textarea>
			<select name="usuario" id="">
				<option value="">Escoja el Usuario</option>
				<?php 
					$con = conexion('root', '');
					$consulta = $con->prepare("select usuario from usuarios where role = 'usuario' "); 
					$consulta->execute();
					$resultado = $consulta->fetchAll();
					$length = count($resultado);
					foreach($resultado as $res){
						echo "<option>".$res['usuario']."</option>";
					}
				?>					
			</select>
			<input type="submit" value="Crear" name="crear" class="log-btn">
		</form>
	</div>
</body>
</html>